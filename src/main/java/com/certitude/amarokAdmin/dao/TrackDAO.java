package com.certitude.amarokAdmin.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.certitude.amarokAdmin.util.DBConnectionMySql;


public class TrackDAO {
	
	public static void doClean(){
		DBConnectionMySql mysql = DBConnectionMySql.getInstance();
		String sql = "select S.id, U.rpath, S.url from statistics S left join urls U on U.id=S.url where rating=2";
		Connection con = null;
		try{
			con = mysql.getConnection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			while(rs.next()){
				int codigo = rs.getInt(1);
				String path = rs.getString(2);
				int url = rs.getInt(3);
					/////Remove actual file////
			    	try{
			    	      
			            String tempFile = path.replace("./musicCentral/", "/Users/KenyiMetal/Desktop/tmp/Music/");
			            //Delete if tempFile exists
			            File fileTemp = new File(tempFile);
			              if (fileTemp.exists()){
			                 if(fileTemp.delete()){
			                	 ///////DELETE TRACK ON DATABASE/////////
			     				Statement stmt = null;
			     				stmt = con.createStatement();
			     				String sql_d="";
			     				int resDel=0;
			     				
			     			    sql_d = "DELETE FROM tracks     WHERE id = "+codigo;
			     			    resDel = resDel + stmt.executeUpdate(sql_d);
			     			    //
			     			    sql_d = "DELETE FROM statistics WHERE id = "+codigo;
			     			    resDel = resDel + stmt.executeUpdate(sql_d);
			     			    //
			     			    sql_d = "DELETE FROM urls       WHERE id = "+url;
			     			    resDel = resDel + stmt.executeUpdate(sql_d);
			     			    //
			     			    if (resDel>=3) {
									System.out.println(tempFile);
								}
			                 }
			              }   
			          }catch(Exception e){
			             // if any error occurs
			             e.printStackTrace();
			          }
			    
				////////////////////////////////////////
			}
			rs.close();
			stm.close();
		}catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
		}finally{
			try{con.close();}catch(Exception exc){};
		}
	}

}
