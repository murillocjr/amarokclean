package com.certitude.amarokAdmin.util;

public class Constant {
	public static final String DB_DRIVE_CLASS_MYSQL = "com.mysql.jdbc.Driver";
	public static final String DB_URL_MYSQL = "jdbc:mysql://127.0.0.1:3307/amarokdb";
	public static final String DB_USER_MYSQL = "root";
	public static final String DB_PASSWORD_MYSQL = "PASSWORD";
	
	public static final int POOL_INIT_SIZE = 3;
	public static final int POOL_INCREMENT = 1;
	public static final int POOL_MAX_SIZE = 100;
}

