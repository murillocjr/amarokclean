package com.certitude.amarokAdmin.util;


//import java.beans.PropertyVetoException;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DBConnectionMySql {
	
	private static DBConnectionMySql dbConnectionMySql = null;
	private ComboPooledDataSource cpds = null;
	
	
	private DBConnectionMySql(){
		cpds = new ComboPooledDataSource();
		try{
			cpds.setDriverClass( Constant.DB_DRIVE_CLASS_MYSQL ); //loads the jdbc driver            
			cpds.setJdbcUrl( Constant.DB_URL_MYSQL );
			cpds.setUser(Constant.DB_USER_MYSQL);                                  
			cpds.setPassword(Constant.DB_PASSWORD_MYSQL);                                  
				
			// the settings below are optional -- c3p0 can work with defaults
			//cpds.setMinPoolSize(3);
			cpds.setInitialPoolSize(Constant.POOL_INIT_SIZE);
			cpds.setAcquireIncrement(Constant.POOL_INCREMENT);
			cpds.setMaxPoolSize(Constant.POOL_MAX_SIZE);
			
		}catch(PropertyVetoException pve){
			pve.printStackTrace();
		}
	}
	
	
	public static DBConnectionMySql getInstance(){
		 if(dbConnectionMySql == null){
			dbConnectionMySql = new DBConnectionMySql(); 
		 }
		 return dbConnectionMySql;
	}
	
	public Connection getConnection(){
		Connection con = null;
		try{
			///
//			System.out.println(cpds.getNumBusyConnections());
//			System.out.println(cpds.getNumConnections());
//			System.out.println(cpds.getNumIdleConnections());
//			System.out.println(cpds.getNumUnclosedOrphanedConnections());
			///
			con = cpds.getConnection();


//		      InitialContext context = new InitialContext();
//		      DataSource dataSource = (DataSource) context.lookup("java:comp/env/jdbc/dbfuxionfac");
//		      con = dataSource.getConnection();
		   
		 } catch (SQLException e) {
		      e.printStackTrace();
		    }
		return  con;
	}
	
}

